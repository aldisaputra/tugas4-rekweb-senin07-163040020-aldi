<style>
#logout{
    top: 50%;
  left: 50%;
transform: translate(-50%, -50%);
float:right;
padding:5px;
border:dashed 1px gray;
margin-top: -168px;
}

.error_msg{
color:red;
font-size: 16px;
}

.message{
top: 10%;
left: 50%;
transform: translate(-50%, -50%);
position: absolute;
font-weight: bold;
font-size: 28px;
color: #6495ED;
width: 500px;
text-align: center;
}
.centered {
  position: fixed;
  top: 50%;
  left: 50%;
  width: 400px;
  /* bring your own prefixes */
  transform: translate(-50%, -50%);
}
</style>
<html>
<?php
if (isset($this->session->userdata['logged_in']['id_user'])) {

    header("location:".base_url());
}
?>
<head>
    <title>Sign In</title>
   <!-- Bootstrap core CSS -->
  <link href="<?= base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url();?>/assets/css/mdb.min.css" rel="stylesheet">
</head>
<body>
<?php
    if (isset($logout_message)) {
        echo "<div class='message'>";
        echo $logout_message;
        echo "</div>";
    }
?>
<?php
    if (isset($message_display)) {
        echo "<div class='message'>";
        echo $message_display;
        echo "</div>";
    }
?>
    <div class="text-center border border-light p-5 centered">
            <?php echo form_open('auth/user_login_process'); ?>
            <?php
                echo "<div class='error_msg'>";
                if (isset($error_message)) {
                echo $error_message;
                }
                echo validation_errors();
                echo "</div>";
            ?>
            <p class="h4 mb-4">Sign in</p>

            <!-- Email -->
            <input type="text" id="username" name="username" class="form-control mb-4" placeholder="Username">

            <!-- Password -->
            <input type="password" id="password" name="password" class="form-control mb-4" placeholder="Password">

            <!-- Sign in button -->
            <button class="btn btn-info btn-block my-4" name="submit" type="submit">Sign in</button>

            <!-- Register -->
            <p>Not a member?
            <a href="<?php echo base_url() ?>index.php/auth/user_registration_show">Register</a>
            </p>
    </div>
</body>
</html>


<!-- Default form login -->


    


<!-- Default form login -->