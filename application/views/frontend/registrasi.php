<html>
<?php
if (isset($this->session->userdata['logged_in'])) {
header("location:".base_url());
}
?>
<style>
.centered {
  position: fixed;
  top: 50%;
  left: 50%;
  width: 400px;
  /* bring your own prefixes */
  transform: translate(-50%, -50%);
}
</style>
<head>
<title>Registration Form</title>
 <!-- Bootstrap core CSS -->
 <link href="<?= base_url();?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url();?>/assets/css/mdb.min.css" rel="stylesheet">
</head>
<body>
<?php
    if (isset($message_display)) {
        echo "<div class='message'>";
        echo $message_display;
        echo "</div>";
    }
?>
<!-- Default form register -->
<form method="post" action="<?= base_url('auth/registration') ?>" class="text-center border border-light p-5 centered">
            <?php
                echo "<div class='error_msg'>";
                if (isset($error_message)) {
                echo $error_message;
                }
                echo validation_errors();
                echo "</div>";
            ?>

    <p class="h4 mb-4">Sign up</p>

    <div class="form-row mb-4">
        <div class="col">
            <!-- First name -->
            <input type="text" id="nama_depan" name="nama_depan" class="form-control" placeholder="First name">
        </div>
        <div class="col">
            <!-- Last name -->
            <input type="text" id="nama_belakang" name="nama_belakang" class="form-control" placeholder="Last name">
        </div>
    </div>

    <!-- E-mail -->
    <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">
    
    <!-- Username -->
    <input type="text" id="username" name="username" class="form-control mb-4" placeholder="Username">

    <!-- Password -->
    <input type="password" id="password" name="password" class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock">
    <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
        At least 8 characters
    </small>

    <!-- Sign up button -->
    <button class="btn btn-info my-4 btn-block" type="submit">Sign Up</button>

    <hr>

    <!-- Terms of service -->
    <p>By clicking
        <em>Sign up</em> you agree to our
        <a href="#" target="_blank">terms of service</a>

</form>
<!-- Default form register -->
</body>
</html>