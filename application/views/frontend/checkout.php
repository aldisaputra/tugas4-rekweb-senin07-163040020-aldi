<?php
if (!isset($this->session->userdata['logged_in']['id_user'])) {
  header("location:".base_url());
}
?>
 <!--Main layout-->
  <main class="mt-5 pt-4">
    <div class="container wow fadeIn">

      <!-- Heading -->
      <h2 class="my-5 h2 text-center">Checkout form</h2>
      <h2 class="my-5 h2 text-center"><?= $this->session->flashdata('message'); ?></h2>
      
      <!--Grid row-->
      <div class="row">

        <!--Grid column-->
        <div class="col-md-8 mb-4">

          <!--Card-->
          <div class="card">

            <!--Card content-->
            <form class="card-body" method="post" action="<?= base_url('web/save_customer') ?>">

              <!--Grid row-->
              <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-2">

                  <!--firstName-->
                  <div class="md-form ">
                    <input type="text" id="nama_depan" name="nama_depan" class="form-control">
                    <label for="nama_depan" class="">First name</label>
                  </div>

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6 mb-2">

                  <!--lastName-->
                  <div class="md-form">
                    <input type="text" id="nama_belakang" name="nama_belakang" class="form-control">
                    <label for="nama_belakang" class="">Last name</label>
                  </div>

                </div>
                <!--Grid column-->

              </div>
              <!--Grid row-->


              <!--email-->
              <div class="md-form mb-5">
                <input type="text" id="email" name="email" class="form-control" placeholder="youremail@example.com">
                <label for="email" class="">Email</label>
              </div>

              <!--email-->
              <div class="md-form mb-5">
                <input type="number" id="no_hp" name="no_hp" class="form-control" placeholder="089xxxxxx">
                <label for="no_hp" class="">Mobile Phone</label>
              </div>

              <!--address-->
              <div class="md-form mb-5">
                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="1234 Main St">
                <label for="alamat" class="">Address</label>
              </div>

              <hr>

              <hr class="mb-4">
              <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>

            </form>

          </div>
          <!--/.Card-->

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-4 mb-4">
        
          <!-- Heading -->
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill"><?php echo $this->cart->total_items();?></span>
          </h4>

           <?php foreach ($this->cart->contents() as $cart_items) ?>
          <!-- Cart -->
          <ul class="list-group mb-3 z-depth-1">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0"><?php echo $cart_items['name'] ?></h6>
                <small class="text-muted">Quantity : <?php echo $cart_items['qty'] ?></small>
              </div>
              <span class="text-muted">Rp. <?php echo $this->cart->format_number($cart_items['subtotal']) ?></span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <span>Total</span>
              <strong>Rp. <?php echo $this->cart->format_number($this->cart->total()); ?></strong>
            </li>
          </ul>
          <!-- Cart -->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->
