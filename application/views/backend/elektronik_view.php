

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Electronics
        <small>Your Vision, Our Future</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
            </div>
            <?php echo $this->session->flashdata('pesan')?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <button class="btn btn-lg btn-primary fa fa-plus" style="margin-bottom:15px;" class="btn btn-primary" data-toggle="modal" data-target="#tambahModal" id="tambah" name="tambah"> Tambah Produk</button>

                <thead>
                <tr>
                  <th >No</th>
                  <th>Nama Produk</th>
                  <th>Merk</th>
                  <th>Kategori</th>
                  <th>Warna</th>
                  <th>Stok</th>
                  <th>Harga</th>
                  <th>Gambar</th>
                  <th>Aksi</th>
                </tr>
                </thead>

                <tbody>
                  <?php $i = 1; ?>
                  <?php if (!empty($elektronik)) : ?>
                  <?php foreach ($elektronik as $key): ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $key->nama_produk; ?></td>
                  <td><?= $key->merk; ?></td>
                  <td><?= $key->kategori; ?></td>
                  <td><?= $key->warna; ?></td>
                  <td><?= $key->stok; ?></td>

                  <td><?="Rp. ". number_format($key->harga,0,',','.'); ?></td>
                  <td>
                    <img src="<?= base_url('assets/img/resize/') ?><?php echo $key->gambar ?>"
                         style="width: 80px; height: 80px;">
                  </td>
                      
                  
                  <!-- edit -->
                  <td align="center">
                    <button type="button" class="fa fa-edit btn btn-warning"  data-toggle="modal" data-target="#ubahModal<?= $key->id ?>" id="ubah" name="ubah"></button>
                    &nbsp; | &nbsp;
                    <a href="<?= base_url('elektronik/delete/'.$key->id); ?>" onclick="return confirm('Apakah anda yakin ?')">
                      <button type="button" class="fa fa-trash btn btn-danger" style="margin-left: -3px;
              margin-top: -3px;"></button>
                </a>
              </td>
            </tr>
        <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="10"><h3>Data yang dicari tidak ditemukan</h3></td>
          </tr>
        <?php endif;?>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- TAMBAH TAMBAH TAMBAH TAMBAH TAMBAH -->


<div id="tambahModal" class="modal modal-info fade bd-example-modal-lg">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Tambah Produk</h4>
      </div>
 <!--modal header-->
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="box-body">
            <form action="<?= base_url('elektronik/insert') ?>" method="post" enctype="multipart/form-data" id="tambahform">
            <div class="form-group">
              <label class="col-sm-1  control-label">Nama Produk</label>
              <div class="col-sm-11"><input type="text" class="form-control " id="nama_produk" name="nama_produk" value="" placeholder="Isi nama produk..">
              </div>
            </div>
          <div class="form-group">
            <label class="col-sm-1  control-label">Merk</label>
            <div class="col-sm-11">
                <select class="form-control select2" style="width: 100%;" data-placeholder="Pilih Merk" id="merk" name="merk">
                 <?php foreach ($merk as $key): ?>
                    <option value="<?= $key->id_merk ?>"><?= $key->nama_merk; ?></option>
                  <?php endforeach ?>
                </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label">Kategori</label>
            <div class="col-sm-11">
               <select class="form-control select2" style="width: 100%;" data-placeholder="Pilih Kategori" id="kategori" name="kategori">
                  <?php foreach ($kategori as $key): ?>
                    <option value="<?= $key->id_kategori ?>"><?= $key->nama_kategori; ?></option>
                  <?php endforeach ?>
                </select>
            </div>
          </div>
           <div class="form-group">
              <label class="col-sm-1  control-label">Warna</label>
              <div class="col-sm-11"><input type="text" class="form-control " id="warna" name="warna" value="" placeholder="Ex: Hitam">
              </div>
           </div>
           <div class="form-group">
            <label class="col-sm-1  control-label">Stok</label>
            <div class="col-sm-11">
            <input type="text" class="form-control decimal" id="stok" name="stok" value="0" placeholder=""> </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1  control-label">Harga</label>
            <div class="col-sm-11">
              <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="text" class="form-control money" id="harga" name="harga" placeholder=""></div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-1  control-label">Gambar</label>
                <div class="col-sm-11">
                  <div class="input-group">
                    <input type="file" name="foto" >
                      <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Pilih satu foto" readonly="">
                      </div>
                  </div>
                </div>
            </div>
            <div class="form-group">
            <label class="col-sm-1  control-label">Deskripsi</label>
            <div class="col-sm-11">
            <input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder=""> </div>
          </div>
            <div class="form-group">
              <label class="col-sm-1  control-label"></label>
              <div class="col-sm-11">
                <button type="submit" title="Save Button" class="btn btn-primary "><i class="fa fa-paper-plane"></i> Save</button> <span id="infoproses"></span>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
 <div class="modal-footer">
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 </div>
 <!--modal footer-->
 </div>
 <!--modal-content-->
 </div>
 <!--modal-dialog modal-lg-->
 </div>


<!-- UBAH UBAH UBAH UBAH UBAH -->

<?php foreach ($elektronik as $key) : ?>
<div id="ubahModal<?= $key->id ?>" class="modal modal-info fade bd-example-modal-lg">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Tambah Produk</h4>
      </div>
 <!--modal header-->
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="box-body">
            <form action="<?= base_url('elektronik/update') ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
              <input type="hidden" id="id" name="id" class="form-control" value="<?= $key->id ?>">
            </div>
            <div class="form-group">
              <label class="col-sm-1  control-label">Nama Produk</label>
              <div class="col-sm-11"><input type="text" class="form-control " id="nama_produk" name="nama_produk" value="<?= $key->nama_produk ?>" placeholder="Please fill out item name">
              </div>
            </div>
          <div class="form-group">
            <label class="col-sm-1  control-label">Merk</label>
            <div class="col-sm-11">
                <select class="form-control select2" style="width: 100%;" data-placeholder="Pilih Merk" name="merk" id="merk">
                 <?php foreach ($merk as $mk): ?>
                    <option value="<?= $mk->id_merk ?>"><?= $mk->nama_merk; ?></option>
                  <?php endforeach ?>
                </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label">Kategori</label>
            <div class="col-sm-11">
               <select class="form-control select2" style="width: 100%;" data-placeholder="Pilih Kategori" name="kategori" id="kategori">
                  <?php foreach ($kategori as $kt): ?>
                    <option value="<?= $kt->id_kategori ?>"><?= $kt->nama_kategori; ?></option>
                  <?php endforeach ?>
                </select>
            </div>
          </div>
           <div class="form-group">
              <label class="col-sm-1  control-label">Warna</label>
              <div class="col-sm-11"><input type="text" class="form-control " id="warna" name="warna" value="<?= $key->warna ?>" placeholder="Ex: Hitam">
              </div>
           </div>
           <div class="form-group">
            <label class="col-sm-1  control-label">Stok</label>
            <div class="col-sm-11"><input type="text" class="form-control decimal" name="stok" id="stok" name="stok" value="<?= $key->stok ?>" placeholder=""> </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1  control-label">Price</label>
            <div class="col-sm-11">
              <div class="input-group">
                <span class="input-group-addon">Rp.</span>
                <input type="text" class="form-control money" id="harga" name="harga" value="<?= $key->harga ?>" placeholder=""></div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-1  control-label">Gambar</label>
                <div class="col-sm-11">
                  <div class="input-group">
                    <input type="file" name="foto">
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text" placeholder="Upload one or more files" readonly="">
                </div>
              </div>
            </div>
            </div>
            <div class="form-group">
              <label class="col-sm-1  control-label">Deskripsi</label>
              <div class="col-sm-11">
                <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="<?= $key->deskripsi ?>"></div>
              </div>
          </div>
            <div class="form-group">
              <label class="col-sm-1  control-label"></label>
              <div class="col-sm-11">
                <button type="submit" title="Save Button" class="btn btn-primary " id="btnsaveitem" name="submit"><i class="fa fa-save"></i> Save</button> <span id="infoproses"></span>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
 <div class="modal-footer">
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 </div>
 <!--modal footer-->
 </div>
 <!--modal-content-->
 </div>
 <!--modal-dialog modal-lg-->
 </div>
 <?php endforeach; ?>