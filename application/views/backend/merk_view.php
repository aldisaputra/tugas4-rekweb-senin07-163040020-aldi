

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Electronics
        <small>Your Vision, Our Future</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
            </div>
            <?php echo $this->session->flashdata('pesan')?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <button class="btn btn-lg btn-primary fa fa-plus" style="margin-bottom:15px;" data-toggle="modal" data-target="#tambahModal" id="tambah" name="tambah"> Tambah Merk</button>

                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Merk</th>
                  <th>Aksi</th>
                </tr>
                </thead>

                <tbody>
                  <?php $i = 1; ?>
                  <?php if (!empty($merk)) : ?>
                  <?php foreach ($merk as $key): ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $key->nama_merk; ?></td>
                                                   
                  <!-- edit -->
                  <td align="center">
                      <button type="button" class="fa fa-edit btn btn-warning"  data-toggle="modal" data-target="#ubahModal<?= $key->id_merk ?>" id="ubah" name="ubah"></button>
                    &nbsp; | &nbsp;
                    <a href="<?= base_url('Merk/delete/'.$key->id_merk); ?>" onclick="return confirm('Apakah anda yakin ?')">
                      <button type="button" class="fa fa-trash btn btn-danger" style="margin-left: -3px;
              margin-top: -3px;"></button>
                </a>
              </td>
            </tr>
        <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="10"><h3>Data yang dicari tidak ditemukan</h3></td>
          </tr>
        <?php endif;?>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<!-- TAMBAH TAMBAH TAMBAH TAMBAH TAMBAH -->


<div id="tambahModal" class="modal modal-success fade bd-example-modal-lg">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Tambah Merk</h4>
      </div>
 <!--modal header-->
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="box-body">
            <form action="<?= base_url('Merk/insert') ?>" method="post" class="form-inline">
            <div class="form-group">
              <label class="col-sm-2">Merk</label>
              <div class="col-sm-11">
                <input type="text" class="form-control" id="merk" name="merk" value="" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-1  control-label"></label>
              <div class="col-sm-11">
                <button type="submit" class="btn btn-primary "><i class="fa fa-save"></i> Save</button>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
 <div class="modal-footer">
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 </div>
 <!--modal footer-->
 </div>
 <!--modal-content-->
 </div>
 <!--modal-dialog modal-lg-->
 </div>



<!-- UBAH UBAH UBAH UBAH UBAH UBAH UBAH -->

<?php foreach ($merk as $key) : ?>
<div id="ubahModal<?= $key->id_merk ?>" class="modal modal-success fade bd-example-modal-lg">
  <div class="modal-dialog modal-sm">
    <div class="modal-content"">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Ubah Merk</h4>
      </div>
 <!--modal header-->
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="box-body">
            <form action="<?= base_url('Merk/update') ?>" method="post" class="form-inline">
            <div class="form-group">
              <input type="hidden" id="id_merk" name="id_merk" class="form-control" value="<?= $key->id_merk ?>">
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-2">Merk</label>
              <div class="col-sm-11">
                <input type="text" class="form-control" id="merk" name="merk" value="<?= $key->nama_merk ?>" placeholder="">
              </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-1  control-label"></label>
              <div class="col-sm-11">
                <button type="submit" class="btn btn-primary "><i class="fa fa-save"></i> Save</button>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
 <div class="modal-footer">
 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 </div>
 <!--modal footer-->
 </div>
 <!--modal-content-->
 </div>
 <!--modal-dialog modal-lg-->
 </div>
 <?php endforeach; ?>