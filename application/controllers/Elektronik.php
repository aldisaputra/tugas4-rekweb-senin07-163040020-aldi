<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elektronik extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Senin07-163040020-Aldi/";
	}

	public function index()
	{
		$data['elektronik'] = json_decode($this->curl->simple_get($this->API . '/Elektronik'));
		$data['kategori'] = json_decode($this->curl->simple_get($this->API . '/Kategori'));
		$data['merk'] = json_decode($this->curl->simple_get($this->API . '/Merk'));

		$this->load->view('backend/templates/header');
		$this->load->view('backend/templates/sidebar');
		$this->load->view('backend/elektronik_view', $data);
		$this->load->view('backend/templates/footer');
	}

	public function insert()
	{
		
		$this->load->library('upload');

		$file = "img_" . time();
		$con['upload_path'] = './assets/img/';
		$con['allowed_types'] = 'gif|bmp|jpg|png|jpeg';
		$con['max_size'] = 3000;
		$con['max_width'] = 1928;
		$con['max_height'] = 1024;
		$con['file_name'] = $file;
		
		$this->upload->initialize($con);

		$name = $_FILES['foto']['name'];
		if (isset($name)) {
			if ($this->upload->do_upload('foto')) {
				$img = $this->upload->data();
				$data = array(
					'gambar' => $img['file_name'],
					'nama_produk' => $this->input->post('nama_produk'),
					'merk' => $this->input->post('merk'),
					'kategori' => $this->input->post('kategori'),
					'warna' => $this->input->post('warna'),
					'harga' => $this->input->post('harga'),
					'stok' => $this->input->post('stok'),
					'deskripsi' => $this->input->post('deskripsi')
					);

				$this->curl->simple_post($this->API . '/Elektronik', $data, array(CURLOPT_BUFFERSIZE => 10));

				$con2['image_library'] = 'gd2';
				$con2['source_image'] = $this->upload->upload_path . $this->upload->file_name;
				$con2['new_image'] = './assets/img/resize/';
				$con2['maintain_ratio'] = TRUE;
				$con2['width'] = 100;
				$con2['height'] = 100;
				$this->load->library('image_lib', $con2);

				if (!$this->image_lib->resize()) {
					$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Success : </b> Produk berhasil ditambahkan
				</div>
			</div>");
				redirect('Elektronik/');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Produk gagal ditambahkan
				</div>
			</div>");
				redirect('Elektronik/');
			}
		}

	}

	public function update()
	{
		
		$this->load->library('upload');

		$data['nama_produk'] = $this->input->post('nama_produk');
		$data['merk'] = $this->input->post('merk');
		$data['kategori'] = $this->input->post('kategori');
		$data['warna'] = $this->input->post('warna');
		$data['stok'] = $this->input->post('stok');
		$data['harga'] = $this->input->post('harga');
		$data['deskripsi'] = $this->input->post('deskripsi');
		$data['id'] =$this->input->post('id');
		$status = $this->curl->simple_put($this->API.'/Elektronik/update', $data, array(CURLOPT_BUFFERSIZE => 10));
		
		$file = "img_" . time();
		$con['upload_path'] = './assets/img/';
		$con['allowed_types'] = 'gif|bmp|jpg|png|jpeg';
		$con['max_size'] = 3000;
		$con['max_width'] = 1928;
		$con['max_height'] = 1024;
		$con['file_name'] = $file;
		
		$this->upload->initialize($con);


		if ($_FILES['foto']['name']) {
			if ($this->upload->do_upload('foto')) {
				$img = $this->upload->data();
				$data['gambar'] = $img['file_name'];
				$status = $this->curl->simple_put($this->API.'/Elektronik', $data, array(CURLOPT_BUFFERSIZE => 10));

				$con2['image_library'] = 'gd2';
				$con2['source_image'] = $this->upload->upload_path . $this->upload->file_name;
				$con2['new_image'] = './assets/img/resize/';
				$con2['maintain_ratio'] = TRUE;
				$con2['width'] = 100;
				$con2['height'] = 100;
				$this->load->library('image_lib', $con2);

				if (!$this->image_lib->resize()) {
					$this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
				}
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Data Berhasil diubah
				</div>
			</div>");
				redirect('Elektronik/');
			} else {
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal diubah
				</div>
			</div>");
				redirect('Elektronik/');
			}
		}
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Data Berhasil diubah
				</div>
			</div>");
			redirect('Elektronik/');
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal diubah
				</div>
			</div>");
			redirect('Elektronik/');
		}
	}

	public function delete($id)
	{
		$status = $this->curl->simple_delete($this->API.'/Elektronik', array('id'=>$id), array(CURLOPT_BUFFERSIZE => 10));
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>X</span>
					</button>
					<b>Success : </b> Data Berhasil dihapus
				</div>
			</div>");
			redirect("Elektronik/");
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal dihapus
				</div>
			</div>");
			redirect("Elektronik/");
		}
	}
}
