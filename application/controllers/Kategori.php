<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Senin07-163040020-Aldi/";
	}

	public function index(){
		$data['kategori'] = json_decode($this->curl->simple_get($this->API . '/Kategori'));

		$this->load->view('backend/templates/header');
		$this->load->view('backend/templates/sidebar');
		$this->load->view('backend/kategori_view', $data);
		$this->load->view('backend/templates/footer');
	}

	public function insert(){
		$data = array(
			'nama_kategori' => $this->input->post('kategori')
        );

		$status = $this->curl->simple_post($this->API . '/kategori', $data, array(CURLOPT_BUFFERSIZE => 10));
		if ($status) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Success : </b> Kategori berhasil ditambahkan
				</div>
			</div>");
			redirect("Kategori");
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"material-icons\">error_outline</i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>X</span>
					</button>
					<b>Error : </b> Kategori gagal ditambahkan
				</div>
			</div>");
			redirect("Kategori");
		}
	}

	public function delete($id_kategori){
		$status =$this->curl->simple_delete($this->API.'/kategori', array('id_kategori'=>$id_kategori), array(CURLOPT_BUFFERSIZE => 10));
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>X</span>
					</button>
					<b>Success : </b> Data Berhasil dihapus
				</div>
			</div>");
			redirect("Kategori");
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Data gagal dihapus
				</div>
			</div>");
			redirect("Kategori");
		}
	}


	public function update(){
		$data = array(
			'id_kategori' => $this->input->post('id_kategori'),
			'nama_kategori' => $this->input->post('kategori')
		);
		$status = $this->curl->simple_put($this->API.'/kategori', $data, array(CURLOPT_BUFFERSIZE => 10));
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Data Berhasil diubah
				</div>
			</div>");
			redirect('Kategori');
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Kategori gagal ditambahkan
				</div>
			</div>");
			redirect('Kategori');
		}
	}
}
