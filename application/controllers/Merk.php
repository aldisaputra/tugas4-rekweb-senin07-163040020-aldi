<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Senin07-163040020-Aldi/";
	}

	public function index(){
		$data['merk'] = json_decode($this->curl->simple_get($this->API . '/Merk'));

		$this->load->view('backend/templates/header');
		$this->load->view('backend/templates/sidebar');
		$this->load->view('backend/merk_view', $data);
		$this->load->view('backend/templates/footer');
	}

	public function insert(){
		$data = array(
			'id_merk' => $this->input->post('id'),
			'nama_merk' => $this->input->post('merk')
		);

		$status = $this->curl->simple_post($this->API . '/Merk', $data, array(CURLOPT_BUFFERSIZE => 10));
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Success : </b> Merk berhasil ditambahkan
				</div>
			</div>");
			redirect('merk/');
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Merk gagal ditambahkan
				</div>
			</div>");
			redirect('merk/');
		}
	}

	public function delete($id_merk){
		$status = $this->curl->simple_delete($this->API.'/merk', array('id_merk'=>$id_merk), array(CURLOPT_BUFFERSIZE => 10));
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-info\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>X</span>
					</button>
					<b>Success : </b> Merk Berhasil dihapus
				</div>
			</div>");
			redirect('merk/');
		}else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Merk gagal dihapus
				</div>
			</div>");
			redirect('merk/');
		}
	}

	public function update(){
		$data = array(
			'id_merk' => $this->input->post('id_merk'),
			'nama_merk' => $this->input->post('merk')
		);
		$status = $this->curl->simple_put($this->API.'/Merk', $data, array(CURLOPT_BUFFERSIZE => 10));
		if ($status >= 0) {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success\">
			<div class=\"container-fluid\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
				<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
			</button>
					<div class=\"alert-icon\">
						<i class=\"fa fa-info-circle\"></i>
					</div>
					
					<b>Success : </b> Merk Berhasil diubah
				</div>
			</div>");
			redirect('Merk/');
		} else {
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger\">
			<div class=\"container-fluid\">
					<div class=\"alert-icon\">
						<i class=\"fa fa-exclamation\"></i>
					</div>
					<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">
						<span aria-hidden=\"true\"><i class=\"material-icons\"></i>x</span>
					</button>
					<b>Error : </b> Merk gagal diubah
				</div>
			</div>");
			redirect('Merk/');
		}
	}
}
