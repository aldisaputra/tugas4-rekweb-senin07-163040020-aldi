<?php

class Kategori_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function getKategori($id_kategori = null){
		$this->db->select('*');
		$this->db->from('tb_kategori');

		if ($id_kategori != null) {
			$this->db->where('id_kategori', $id_kategori);
		}

		return $this->db->get();
	}

	public function insert($data){
       $this->db->insert('tb_kategori', $data);
       return TRUE;
    }

	public function delete($table, $par, $var){
		$this->db->where($par, $var);
		$this->db->delete($table);
	}

	public function update($id, $data){
		$this->db->where('id_kategori', $id);
		$this->db->update('tb_kategori', $data);
	}
}
